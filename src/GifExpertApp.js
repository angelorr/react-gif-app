import React, {useState} from 'react';
import AddCategory from './components/AddCategory';
import GifGrid from './components/GifGrid';

//7UjrzlPOJuTtwT0Iffr3GKx0zAV6HPe9

const GifExpertApp = () => {

	//const categories = ['One Piece', 'Samurai X', 'Hajime no Ippo'];
	const [categories, setCategories] = useState(['']);

	return (
		<div>
			<h2>GifExpertApp</h2>
			<AddCategory setCategories={setCategories}/>
			<hr />

			<ol>
				{ 
					categories.map((category) => (
						<GifGrid 
							key={category} 
							category={category} 
						/>
					))
				}
			</ol>
		</div>
	)
}

export default GifExpertApp;